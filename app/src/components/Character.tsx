import React from 'react'
import InputField from './InputField'
import TraitField from './TraitField';
import { newCharacter } from '../data/newCharacter';

function Character(): JSX.Element {
  let { abilities, talents } = newCharacter;

  return (
    <div className='w-full'>
      <form action="" className='flex flex-col gap-4 flex-grow'>
        <InputField label={'Hero Name'} type='text' />
        <InputField label={'Real Name'} type='text' />

        {/* Abilities */}
        <h2 className="font-bold">Abilites</h2>
        {abilities.map(ability => (
         <TraitField label={ability.name} key={ability.name} />
        ))}
        <h2 className="font-bold">Talents</h2>
        {/* Talents */}
        {talents.map(talent => (
         <TraitField label={talent.name} key={talent.name} />
        ))}
        {/* Powers */}
        <h2 className="font-bold">Powers</h2>
        <h2 className="font-bold">Perks</h2>
        <h2 className="font-bold">Flaws</h2>
        <h2 className="font-bold">Gear</h2>

      </form>

    </div>
  )
}

export default Character