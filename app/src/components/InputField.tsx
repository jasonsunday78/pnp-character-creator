import React, { ChangeEvent } from 'react';

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> { 
  label: string; 
}

const InputField: React.FC<InputProps> = ({ label, ...props }) => { 
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => { 
    const { value } = event.target; 
    props.onChange && props.onChange(event); // Call the original onChange event handler if provided 
    
    props.onBlur && props.onBlur(event); // Call the original onBlur event handler if provided 
    props.onFocus && props.onFocus(event); // Call the original onFocus event handler if provided 
    props.onInput && props.onInput(event); // Call the original onInput event handler if provided 
    props.onKeyPress && props.onKeyPress(event); // Call the original onKeyPress event handler if provided 
    // Add additional event handlers here if needed 
  }; 
  return ( 
    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
     {label && <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor={props.id}>{label}</label>}
      <input {...props} 
      className='shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
      onChange={handleInputChange} /> 
    </div>   
  ); 
};

export default InputField;