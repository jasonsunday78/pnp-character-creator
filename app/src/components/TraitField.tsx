import React, { ChangeEvent } from 'react';

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

const TraitField: React.FC<InputProps> = ({ label, ...props }) => {
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    props.onChange && props.onChange(event); // Call the original onChange event handler if provided 

    props.onBlur && props.onBlur(event); // Call the original onBlur event handler if provided 
    props.onFocus && props.onFocus(event); // Call the original onFocus event handler if provided 
    props.onInput && props.onInput(event); // Call the original onInput event handler if provided 
    props.onKeyPress && props.onKeyPress(event); // Call the original onKeyPress event handler if provided 
    // Add additional event handlers here if needed 
  };
  return (
    <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0 inline-flex text-center">
      {label && <label className="block uppercase tracking-wide text-gray-700 text-xl font-bold mx-3 m-auto items-center" htmlFor={props.id}>{label}</label>}
      <input {...props}
        min={1}
        max={24}
        maxLength={2}
        type='number'
        className='w-14 shadow appearance-none border rounded py-2 px-2 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
        onChange={handleInputChange}
      />
      <label className="block uppercase tracking-wide text-gray-700 text-xl font-bold mx-1 m-auto">D</label>
    </div>
  );
};

export default TraitField;