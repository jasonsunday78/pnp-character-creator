import { Character, Power, Pro, Con, Perk, Flaw } from "../types/character"

export const newCharacter: Character = {
    heroName: "",
    realName: "",
    edge: 0,
    health: 0,
    resolve: 0,
    heroPoints: 0,
    abilities: [
        {
            id: 1,
            name: "agility",
            rank: 1,
            description: "agility",
            cost: 1
        },
        {
            id: 2,
            name: "intellect",
            rank: 1,
            description: "intellect",
            cost: 1
        },
        {
            id: 3,
            name: "might",
            rank: 1,
            description: "might",
            cost: 1
        },
        {
            id: 4,
            name: "perception",
            rank: 1,
            description: "perception",
            cost: 1
        },
        {
            id: 5,
            name: "toughness",
            rank: 1,
            description: "toughness",
            cost: 1
        },
        {
            id: 6,
            name: "willpower",
            rank: 1,
            description: "willpower",
            cost: 1
        }
    ],
    talents: [
        {
            id: 1,
            name: "academics",
            rank: 1,
            description: "academics",
            cost: 1
        },
        {
            id: 2,
            name: "charm",
            rank: 1,
            description: "charm",
            cost: 1
        }, 
        {
            id: 3,
            name: "command",
            rank: 1,
            description: "command",
            cost: 1
        }, 
        {
            id: 4,
            name: "covert",
            rank: 1,
            description: "covert",
            cost: 1
        }, 
        {
            id: 5,
            name: "investigation",
            rank: 1,
            description: "investigation",
            cost: 1
        }, 
        {
            id: 6,
            name: "medicine",
            rank: 1,
            description: "medicine",
            cost: 1
        }, 
        {
            id: 7,
            name: "professional",
            rank: 1,
            description: "professional",
            cost: 1
        }, 
        {
            id: 8,
            name: "science",
            rank: 1,
            description: "science",
            cost: 1
        }, 
        {
            id: 9,
            name: "streetwise",
            rank: 1,
            description: "streetwise",
            cost: 1
        }, 
        {
            id: 10,
            name: "survival",
            rank: 1,
            description: "survival",
            cost: 1
        }, 
        {
            id: 11,
            name: "technology",
            rank: 1,
            description: "technology",
            cost: 1
        },
        {
            id: 12,
            name: "vehicles",
            rank: 1,
            description: "vehicles",
            cost: 1
        }
    ],
    powers: [],
    perks: [],
    flaws: [],
    gear: []
}

export const powers: Power[] = [
    {
        name: "",
        rank: 1,
        description: "",
        cost: 1,
        source: "",
        pros: [],
        con: []
    }
];

export const pros: Pro[] =[
    {
        name: "",
        cost: 1,
        description:""    
    }
];

export const cons: Con[] =[
    {
        name: "",
        refund: 1,
        description:""    
    }
];

export const perks: Perk[] =[
    {
        name: "",
        cost: 1,
        description:""    
    }
];

export const flaw: Flaw[] =[
    {
        name: "",
        description:""    
    }
];