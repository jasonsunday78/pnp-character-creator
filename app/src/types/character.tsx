export type Character = {
    heroName: string;
    realName: string;

    edge: number;
    health: number;
    resolve: number;
    heroPoints: number;

    abilities: Ability[];
    talents: Talent[];

    powers: Power[];
    perks: Perk[];
    flaws: Flaw[];
    gear: Gear[];

}


export type Ability = {
    id: number;
    name: string;
    rank: number;
    description: string;
    cost: number;
}

export type Talent = {
    id: number;
    name: string;
    rank: number;
    description: string;
    cost: number;
}

export type Power = {
    name: string;
    rank: number;
    description: string;
    cost: number;
    source: string;
    pros: Pro[];
    con: Con[];
}

export type Pro = {
    name: string;
    cost: number;
    description: string;
}

export type Con = {
    name: string;
    refund: number;
    description: string;
}
export type Perk = {
    name: string;
    description: string;
    cost: number;
}

export type Flaw = {
    name: string;
    description: string;
}

export type Gear = {}